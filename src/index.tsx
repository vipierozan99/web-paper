import React from "react";
import ReactDOM from "react-dom/client";

import Heading from "./components/Heading";
import Layout from "./components/Layout";
import Main from "./pages/main.mdx";

const components = {
  h1: Heading,
  wrapper: Layout,
};

ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  <React.StrictMode>
    <Main components={components} />
  </React.StrictMode>
);
