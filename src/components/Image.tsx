import { ClassAttributes, ImgHTMLAttributes } from "react";

export default function Image(
  props: JSX.IntrinsicAttributes &
    ClassAttributes<HTMLImageElement> &
    ImgHTMLAttributes<HTMLImageElement>
) {
  return <img {...props} />;
}
