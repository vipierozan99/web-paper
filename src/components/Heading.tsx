import { ClassAttributes, HTMLAttributes } from "react";

export default function Heading(
  props: JSX.IntrinsicAttributes &
    ClassAttributes<HTMLHeadingElement> &
    HTMLAttributes<HTMLHeadingElement>
) {
  return <h1 {...props} />;
}
