import { ClassAttributes, HTMLAttributes } from "react";

import "./Layout.css";
import classes from "./Layout.module.css";

export default function Layout({
  children,
  ...props
}: JSX.IntrinsicAttributes &
  ClassAttributes<HTMLElement> &
  HTMLAttributes<HTMLElement>) {
  return (
    <main {...props} className={classes.two_columns_container}>
      {children}
    </main>
  );
}
