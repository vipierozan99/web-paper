import { ClassAttributes, ColHTMLAttributes } from "react";

export default function Column({
  children,
  ...props
}: JSX.IntrinsicAttributes &
  ClassAttributes<HTMLTableColElement> &
  ColHTMLAttributes<HTMLTableColElement>) {
  return (
    <div {...props} style={{ color: "blue" }}>
      {children}
    </div>
  );
}
