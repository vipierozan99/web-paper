declare module "*.png" {
  const value: any;
  export = value;
}

declare module "*.css" {
  const content: { [className: string]: string };
  export default content;
}

declare module "*.module.css";

declare module "*.mdx";
