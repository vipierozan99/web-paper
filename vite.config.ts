import mdx from "@mdx-js/rollup";
import react from "@vitejs/plugin-react";
import * as path from "path";
import rehypeHighlight from "rehype-highlight";
import rehypeMathJax from "rehype-mathjax";
import remarkGfm from "remark-gfm";
import remarkMath from "remark-math";
import { defineConfig } from "vite";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    react(),
    mdx({
      development: true,
      remarkPlugins: [remarkGfm, remarkMath],
      rehypePlugins: [rehypeMathJax, rehypeHighlight],
    }),
  ],
  resolve: {
    alias: [{ find: "@", replacement: path.resolve(__dirname, "src") }],
  },
  assetsInclude: ["src/assets/**/*"],
});
